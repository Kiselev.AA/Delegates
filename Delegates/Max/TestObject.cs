﻿namespace Delegates.Max
{
    public class TestObject
    {
        public int i1;
        public int i2;
        public int i3;
        public int i4;
        public int i5;

        public TestObject()
        {
            i1 = 1;
            i2 = 2;
            i3 = 3;
            i4 = 4;
            i5 = 5;
        }

        public TestObject(int i1, int i2, int i3, int i4, int i5)
        {
            this.i1 = i1;
            this.i2 = i2;
            this.i3 = i3;
            this.i4 = i4;
            this.i5 = i5;
        }

        public override string ToString()
        {
            return "i1 = " + i1.ToString() +
                ", i2 = " + i2.ToString() +
                ", i3 = " + i3.ToString() +
                ", i4 = " + i4.ToString() +
                ", i5 = " + i5.ToString();
        }
    }
}
