﻿using System.Collections;

namespace Delegates.Max
{
    public static class Maximum
    {
        public static void Run(IEnumerable<TestObject> enumer)
        {
            Console.WriteLine("Test collection");
            foreach (var el in enumer)
                Console.WriteLine(el.ToString());
            Console.WriteLine();

            var res = enumer.GetMax<TestObject>(GetValue);
            if (res != null)
            {
                Console.WriteLine("Maximum element of collection: {0}", res.ToString());
            }
            else
            {
                Console.WriteLine("Maximum element of collection not founded.");
            }
            Console.WriteLine();
        }

        public static void Run(List<string> strings)
        {
            Console.WriteLine("Test collection");
            foreach (var el in strings)
            {
                Console.WriteLine(el.ToString());
            }
            Console.WriteLine();

            var res = strings.GetMax<string>(GetValueString);
            if (res != null)
            {
                Console.WriteLine("Maximum element of collection: {0}", res.ToString());
            }
            else
            {
                Console.WriteLine("Maximum element of collection not founded.");
            }
            Console.WriteLine();
        }

        private static float GetValue(TestObject obj)
        {
            return obj.i1 + obj.i2 + obj.i3 + obj.i4 + obj.i5;
        }

        private static float GetValueString(string str)
        {   
            if (str == null)
            {
                return 0;
            }

            float result = 0;
            float razryd = 1;

            for(int i = 0; i < str.Length; i++)
            {
                razryd /= 10;
                result += (float)str[i]*razryd;
            }
            return result;
        }

        private static T? GetMax<T>(this IEnumerable e, Func<T, float> getParameter) where T : class
        {
            List<T> listElements = new List<T>();
            foreach (var element in e)
            {
                listElements.Add((T)element);
            }

            if (listElements.Count == 0)
            {
                return null;
            }

            T res = listElements[0];
            foreach (var element in listElements)
            {
                if (getParameter(res) < getParameter(element))
                {
                    res = element;
                }
            }
            return res;
        }
    }
}
