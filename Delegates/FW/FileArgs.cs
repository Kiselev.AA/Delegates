﻿namespace Delegates.FW
{
    public class FileArgs : EventArgs
    {
        public string FileName { get; set; }

        public FileArgs(string fileName)
        {
            FileName = fileName;
        }
    }
}
