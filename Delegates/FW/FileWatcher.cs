﻿namespace Delegates.FW
{
    public class FileWatcher : IDisposable
    {
        public event EventHandler<FileArgs>? FileFound;
        private bool proc = true;
        private string _path;
        private List<string> files;

        public List<string> Files { get { return files; } }

        public FileWatcher(string path)
        {
            _path = path;
            files = new List<string>();
        }

        public void Start()
        {
            Watch(_path);
        }

        private void Watch(string path)
        {
            if (!proc)
                return;

            foreach (var item in Directory.GetFiles(path))
            {
                files.Add(item);

                var args = new FileArgs(item);
                FileFound?.Invoke(this, args);
            }
            foreach (var item in Directory.GetDirectories(path))
            {
                Watch(item);
            }
        }

        public void Stop()
        {
            proc = false;
        }

        public void Dispose()
        {
            FileFound = null;
        }
    }
}
