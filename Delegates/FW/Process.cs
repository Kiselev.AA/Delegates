﻿namespace Delegates.FW
{
    public static class Process
    {
        public static void Run(string path)
        {
            Console.WriteLine("Start watcher file in {0}:", path);
            Console.WriteLine();

            var fileWatcher = new FileWatcher(path);
            fileWatcher.FileFound += FileWatcher_FileFound;
            fileWatcher.Start();

            Console.WriteLine();
            Console.WriteLine("End work watcher!");
        }

        private static void FileWatcher_FileFound(object? sender, FileArgs e)
        {
            Console.WriteLine(e.FileName);
        }
    }
}
