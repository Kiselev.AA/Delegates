﻿using Delegates.Max;
using Delegates.FW;

namespace Delegates
{
    public static class Program
    {
        static void Main(string[] args)
        {
            Maximum.Run(new List<TestObject>()
            {
                new TestObject(),
                new TestObject(2,4,6,8,10),
                new TestObject(1,3,7,9,11),
                new TestObject(2,4,5,6,12)
            });

            Maximum.Run(new List<string>()
            {
                "Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec"
            });



            Process.Run("c:\\test");
        }
    }
}